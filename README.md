# Pushing the Internet to the Edge

Master thesis of Raphael Schnider, TEC, D-ITET 2020.<br/>
The thesis report can be found in the report/ folder.

## Source Code

In this thesis four network components were implemented: constrained nodes, edge router, directory server and user device. The code is available as open source. The following sections provide an overview of the code structure, more details on how to use each component is given in the appropriate subfolder or repository.

### Directory Server

The implementation for the directory server can be found in the directory-server/ folder. It consists of two files:
* directory_resources.py : implements the device list as a CoAP resource, included by the server application
* directory_server.py : main executable of the directory server, starts the CoAP server and the devices resource

### Edge Router

The edge router consists of a Baseboard and a COM board, interconnected through BOLT. The Baseboard implementation of the edge router implementation can be found in the edge-router/ folder. It consists of four files:
* baseboard_router.py : the main loop of the edge router. Forwards packets between the WAN Ethernet interface and the BOLT interface which connects it to the LPN
* bolt_interface.py : provides the interface functions to read and write from/to BOLT
* sixlowpan.py : provides functions to add or remove the 6LoWPAN layer
* wan_eth_interface.py : provides functions to receive Ethernet packets and parse Ethernet, IP and UDP headers

The COM board and BOLT implementation are the same as for the constrained nodes.

### User Device

A simple demo application executed by the user device can be found in testing/demo-app-user/<br/>
In this demo the user observes the temperature measured on one node and if it rises above 30 degree Celsius, the cooling actuator is triggered on the second node.<br/>
The user code performing the requests for the experiments as decribed in the evaluation chapter of the report (chapter 5) can be found in the testing/experiment-user-code/ folder.

### Constrained Node

The constrained nodes consist of an APP and a COM board, interconnected through BOLT. The same BOLT and COM board hardware & software are used on the edge router
* APP board source code: https://gitlab.ethz.ch/tec/public/students/ma_schnider/contiki-ng-dpp2
* COM board source code: https://gitlab.ethz.ch/tec/public/students/ma_schnider/baloo-dpp2
* BOLT source code: https://github.com/ETHZ-TEC/BOLT . In this thesis, the hex file provided in binary/BOLT_128b_reset.hex is used.

## Evaluation

The code used and results obtained from the evaluation, described in chapter 5 of the report, are available.<br/>
* testing/demo-app-user/ contains the demo application code to observe the temperature on one node and trigger cooling on another if the temperature rises above 30 degree Celsius
* testing/experiment-results/ contains the detailed measurements of success rate and response time for the performed experiments
* testing/experiment-user-code/ contains the user code used for the experiments
	* experiments 1-3 were done using 8 different CoAP request sequences using coapclient_testing_seqX.py
	* experiment 4 used one CoAP request sequence, which was performed by coapclient_testing_seq1.py
	* experiment 5 used one ping request sequence, implemented by ping_test.py
	* experiment 6 used one CoAP request sequence, implemented by coapclient_test_fragmentation.py
* testing/random-request-sequence/ contains the code used for generating the random request sequences, as well as the generated sequences used for the evaluation


The serial output of the APP board of the constrained node can be logged using app_serial_output_to_file.sh, the COM board output can be logged using com_serial_output_to_file.sh