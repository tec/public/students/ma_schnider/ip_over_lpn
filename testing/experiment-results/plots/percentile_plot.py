import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df=pd.DataFrame({'x': [50,75,90], 'static': [2.093,2.393,2.582], 'mobile':[2.105,2.431,2.539]})

plt.plot('x','static', data=df, marker='o',markersize=7,linewidth=3, markerfacecolor='blue', color='blue')
plt.plot('x','mobile', data=df, marker='o',markersize=7,linewidth=3, markerfacecolor='red', color='red')
plt.legend()
plt.xlabel("Percentile")
plt.ylabel("Response Time Upper Bound [s]")

plt.savefig("rttpercentiles.png")



