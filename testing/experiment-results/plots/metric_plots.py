import numpy as np
import matplotlib.pyplot as plt

#bin count 30-40 seems to look good
#current figures use 35
def response_time_histogram(input_file, caption, bin_count, exp_number):
	if (not isinstance(input_file, str)):
		raise ValueError("Wrong input type. First parameter must be a string, got "+repr(input_file)+".")

	response_times_array = []
	samples_count = 0

	data_file = open(input_file, "r")

	data_file.seek(0,0)
	line = data_file.readline()

	while((line !='') and (line != '\n')):
		print(line)
		response_times_array.append(float(line))
		samples_count += 1
		line = data_file.readline()


	#hist,bin_edges = np.histogram(response_times_array,bins='auto')
	#print(hist)
	#print(bin_edges)
	#print(samples_count)

	n, bins, patches = plt.hist(x=response_times_array, bins=bin_count, rwidth=0.85, range=(1,10))
	plt.grid(axis='y')
	plt.xlabel("Response Time [s]")
	plt.ylabel("Number of Packets")
	#plt.title(caption)
	plt.ylim(ymax=30)

	#plt.show()
	plt.savefig("rtt_fig_exp" +str(exp_number) + "_notitle.png")

	return


