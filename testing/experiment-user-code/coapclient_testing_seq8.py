from coapthon.client.helperclient import HelperClient
from time import time,sleep
import os
from queue import Queue
from threading import Thread
from timeit import default_timer as timer
from datetime import date,datetime,timezone

timestamp = datetime.now(timezone.utc)
output_file = open("testResults_seq8.txt", "a+")
output_file.write("[" + str(timestamp.ctime()) + "] Test run started\n\r") 

host = "fc::1"
port = 5683
path_discovery = ".well-known/core"
path_temperature = "temp"
#timeout of the coap requests. retry count follows implicitly: 1st retry after approx. 2.5s, exponential back-off(doubling), timing not very accurate. timeout of 15 means last request should be sent at t+7.5s (max 3 tries)
coap_timeout_s = 15

#the generated random intervals between requests, in seconds

Y1_rand_init= 1.400000
Y1=[27.800000,23.000000,24.200000,26.500000,22.100000,26.500000,23.600000,23.600000]
Y2_rand_init= 1.100000
Y2=[27.700000,26.600000,28.500000,21.500000,20.700000,22.700000,25.200000,24.400000]
Y3_rand_init= 1.400000
Y3=[27.200000,26.700000,22.400000,29.000000,23.600000,22.000000,20.400000,26.900000]
Y4_rand_init= 1.800000
Y4=[25.600000,20.700000,20.500000,24.500000,21.700000,20.800000,24.600000,25.000000]
Y5_rand_init= 0.300000
Y5=[29.200000,26.700000,29.400000,29.200000,26.000000,21.200000,21.700000,26.200000]
Y6_rand_init= 0.600000
Y6=[28.300000,24.600000,20.500000,27.600000,22.300000,25.000000,27.100000,23.400000]
Y7_rand_init= 0.400000
Y7=[27.100000,22.800000,23.800000,24.600000,27.100000,28.400000,23.400000,23.800000]
Y8_rand_init= 0.200000
Y8=[28.900000,20.100000,22.100000,27.500000,27.400000,24.900000,26.100000,25.700000]
N=8
#N is the number of intervals. number of requests is n+1

test_sequence = Y8
start_delay_s = 5
request_delay_randomized = Y8_rand_init



#worker num: 0-7
class CoapRequestWorker(Thread):
	def __init__(self, queue, worker_num):
		Thread.__init__(self)
		self.queue = queue
		self.worker_num = worker_num
		wait_time_sec = 0
		for i in range(worker_num):
			wait_time_sec = wait_time_sec + test_sequence[i]
		self.wait_time_sec = wait_time_sec
		self.coap_client = HelperClient(server=(host,port))
		#print("worker " + str(worker_num) + " wait time: " + str(wait_time_sec))
	def run(self):
		sleep(self.wait_time_sec)
		start = timer()
		timestamp = datetime.now(timezone.utc)
		output_file.write("[" + str(timestamp.ctime()) + "] Send request "+ str(self.worker_num+1) + " \n\r") 
		response = self.coap_client.get(path_temperature, timeout=coap_timeout_s)
		end = timer()
		delay = end - start
		success = False
		if(response is None):
			print("worker " + str(self.worker_num) + " failed")
			success = False
		else:
			#print(response.pretty_print())
			print("worker " +str(self.worker_num) + " sucess, delay: " +str(delay))
			success = True

		self.coap_client.stop()
		self.queue.put((self.worker_num, success, delay))


def main():
	queue = Queue()
	worker_list = list()
	success_count = 0
	delay_list = [0] * (N+1)
	jitter_list = [-1] * N
# first wait a bit until system is rdy
	sleep(start_delay_s)
	sleep(request_delay_randomized)
	
	for x in range(N+1):
		worker = CoapRequestWorker(queue, x)
		worker.daemon = True 
		worker.start()
		worker_list.append(worker)
	
	#queue.join()

	for k in worker_list:
		k.join()


	for i in range(N+1):
		request_num,success,delay = queue.get()
		if success is True:
			success_count = success_count + 1
			delay_list[request_num] = delay
		else:
			delay_list[request_num] = -1

	#calculate jitters
	last_delay = delay_list[0]
	next_delay = -1
	for u in range(1,N+1):
		next_delay = delay_list[u]
		if(next_delay == -1):
			continue
		elif(last_delay == -1):
			last_delay = next_delay
			continue
		else:
			jitter_list[u-1] = abs(last_delay - next_delay)
			last_delay = next_delay


	results_string = str(success_count)+"/"+str(N+1) + " successful requests \r\n delays: " + str(delay_list) + " \n\r jitters: " + str(jitter_list)
	print("Test result: " + results_string)
	timestamp = datetime.now(timezone.utc)
	
	output_file.write("[" + str(timestamp.ctime()) + "] " + "Results for sequence: " + str(test_sequence) + "\r\n")
	output_file.write(results_string + "\r\n")

if __name__ == '__main__':
	main()


