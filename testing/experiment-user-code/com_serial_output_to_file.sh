#!/bin/bash

#output_file = "./node_serial_output.txt"

stty -F /dev/ttyUSB1 ispeed 115200 ospeed 115200
echo "New run" >> "node_com_serial_output.txt"
cat -v /dev/ttyUSB1 | ts '[%d-%m-%Y %H:%M:%S]' >> "node_com_serial_output.txt"
