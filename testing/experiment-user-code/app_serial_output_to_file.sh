#!/bin/bash

#output_file = "./node_serial_output.txt"

stty -F /dev/ttyUSB0 ispeed 115200 ospeed 115200
echo "New run" >> "node_app_serial_output.txt"
cat -v /dev/ttyUSB0 | ts '[%d-%m-%Y %H:%M:%S]' >> "node_app_serial_output.txt"
