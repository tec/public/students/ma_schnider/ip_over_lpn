from coapthon.client.helperclient import HelperClient
from time import time,sleep
import os
from queue import Queue
from threading import Thread
from timeit import default_timer as timer
from datetime import date,datetime,timezone
import sys, getopt
import logging



port = 5683
path_discovery = ".well-known/core"
path_temperature = "temp"
path_temperature_observe = "temp_obs"
path_cooling = "cooling"
#timeout of the coap requests. retry count follows implicitly: 1st retry after approx. 2.5s, exponential back-off(doubling), timing not very accurate. timeout of 15 means last request should be sent at t+7.5s (max 3 tries)
coap_timeout_s = 60
not_yet_triggered = True

def client_observe_callback(response):
	global client
	global not_yet_triggered
	print("Observed temperature: " +str(response.payload))
	
	if(int(response.payload) >= 30):
		client.cancel_observing(response, False)
		not_yet_triggered = False

def main(argv):
	logger=logging.getLogger()
	logger.setLevel(logging.INFO)
	global client
	global not_yet_triggered
	temperature_node_address = ''
	cooling_node_address = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:",["temp_node_addr=","cooling_node_addr="])
	except getopt.GetoptError:
		print ("coapclient_demo_tempcontrol.py -i <temp_node_addr> -o <cooling_node_addr>")
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-i", "--input_node_addr"):
			temperature_node_address = arg
		elif opt in ("-o", "--output_node_addr"):
			cooling_node_address = arg
	client = HelperClient(server=(temperature_node_address,port))
	client.observe(path_temperature_observe, client_observe_callback)

	while(not_yet_triggered is True):
		pass

	print("temperature triggered, activate cooling")

	client = HelperClient(server=(cooling_node_address,port))
	cooling_payload = "on"
	response = client.post(path_cooling, cooling_payload)
	print((response.pretty_print()))
	client.stop()


if __name__ == "__main__":
	main(sys.argv[1:])


