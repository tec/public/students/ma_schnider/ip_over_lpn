import numpy.random

N = 8
value_min = 200
value_max = 300
initial_delay_randomized_min=0
initial_delay_randomized_max = 20
integers_per_second = 10
#unit of the values is 0.1s (value of 10 means 1s)
Y = [[0] * N for i in range(N)]

output_file = open("random_sequences.txt", "a+")


for counter_sequence in range(0,N):
	output_file.write("\r\n")
	initial_delay_randomized = numpy.random.randint(low=initial_delay_randomized_min, high=initial_delay_randomized_max+1)
	output_file.write("Y%d_rand_init: " % (counter_sequence+1))
	output_file.write("%f\r\n" % (initial_delay_randomized / integers_per_second))
	for counter_element in range(0,N):
		Y[counter_sequence][counter_element] = numpy.random.randint(low=value_min, high=value_max+1)
		if(counter_element == 0):
			output_file.write("Y%d:[" % (counter_sequence+1))

		output_file.write("%f" % (Y[counter_sequence][counter_element]/integers_per_second))

		if(counter_element == (N-1)):
			output_file.write("]")

		else:
			output_file.write(",")

print(Y)

