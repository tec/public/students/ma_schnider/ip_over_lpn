#WAN Ethernet interface for the baseboard
#
# Author: sraphael
# Date:   11.12.19
# inspired by https://github.com/Daymorelah/packetAnalyzerAndSniffer

import socket
import struct
import binascii
import sixlowpan
import ipaddress

er_public_ipv6_address = ipaddress.IPv6Address("2001:67c:10ec:2a49:8000::23")
cloud_server_ipv6_address = ipaddress.IPv6Address("fc::99")

def wan_interface_init():
    try:
        #wan_socket = socket.socket(socket.AF_INET6, socket.SOCK_RAW, socket.IPPROTO_RAW)
        #wan_socket.bind(('',0))
        wan_socket = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.htons(0x003))
        wan_socket.setblocking(0)
        return wan_socket
    except socket.error as msg:
        print ('Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit()

def etherHeader(packet):
  IpHeader = struct.unpack("!6s6sH",packet[0:14]) #ipv4==0x0800
  dstMac = binascii.hexlify(IpHeader[0]) #source MAC address. converts binary data into ascii dt looks like hex. MAC address is always in hex format.
  srcMac = binascii.hexlify(IpHeader[1]) #Destination MAC address
  protoType = IpHeader[2] #next protocol (ip/ipv4,arp,icmp,ipv6)
  nextProto = hex(protoType) #hex() returns a string. it a built in finction

  #print ("*******************ETHER HEADER***********************")
  #print ("\tDestination MAC: "+str(dstMac[0:2])+":"+str(dstMac[0:2])+":"+str(dstMac[2:4])+":"+str(dstMac[4:6])+":"+str(dstMac[6:8])+":"+str(dstMac[8:10])+":"+str(dstMac[10:]))
  #print ("\tsource MAC: "+str(srcMac[0:2])+":"+str(srcMac[0:2])+":"+str(srcMac[2:4])+":"+str(srcMac[4:6])+":"+str(srcMac[6:8])+":"+str(srcMac[8:10])+":"+str(srcMac[10:]))

  #print ("\tNext Protocol: "+nextProto)
  #print type(nextProto). Turns out nextProto is a string bcos of the hex() which returns a srting

  proto = 'unknown'
  if (nextProto == '0x800'): #IP/IPV4 frame ethertype. check if_ether.h for other ether protocol hex values.
     proto = 'IPV4'
  if (nextProto == '0x806'): #ARP  frame. check wikipedia (ether type)
     proto = 'ARP'
  if (nextProto == '0x86dd'): #IP/IPV6 frame. check if_ethr.h header file
     proto = 'IPV6'

  packet = packet[14:]
  return packet,proto

#returns ipv6 packet
def wan_receive(wan_socket):
    #print ("waiting to receive packet...")
    try:
        raw_packet = wan_socket.recv(2048)
        resultingPacket,proto = etherHeader(raw_packet)
        #print ("ethernet packet: " + str(packet))
        return resultingPacket,proto
    except socket.error as error_msg:
        #print("no new data")
        return None,None

def ipv6_header_parse(packet):
    header = struct.unpack("!1L2H16s16s",packet[0:40])
    version = header[0] >> 28
    traffic_class = (header[0] >> 20) & 0x000000FF
    flow_label = header[0] & 0x000FFFFF
    payload_len = header[1]
    next_hdr = header[2] >> 8
    hop_limit = header[2] & 0x00FF
    src_addr = socket.inet_ntop(socket.AF_INET6, header[3])
    dst_addr = socket.inet_ntop(socket.AF_INET6, header[4])

    #print ("****IPv6 Header****")
    #print ("\tVersion: "+str(version))
    #print ("\tTraffic Class: "+str(traffic_class))
    #print ("\tFlow Label: "+str(flow_label))
    #print ("\tPayload Length: "+str(payload_len))
    #print ("\tNext Header: "+str(next_hdr))
    #print ("\tHop Limit: "+str(hop_limit))
    #print ("\tSource Address: "+src_addr)
    #print ("\tDestination Address: "+dst_addr)
    #no extension header
    transport_layer_datagram = packet[40:]
    return transport_layer_datagram, next_hdr, src_addr, dst_addr

def ipv4_header_parse(packet):
  header = struct.unpack("!1B1H4s4s", packet[9:20])
  next_hdr = header[0]
  header_checksum = header[1]
  src_addr = socket.inet_ntop(socket.AF_INET, header[2])
  dst_addr = socket.inet_ntop(socket.AF_INET, header[3])
  #no extension header
  transport_layer_datagram = packet[20:]
  return transport_layer_datagram, next_hdr, src_addr, dst_addr

def udp_header_parse(datagram):
    header = struct.unpack("!4H", datagram[0:8])
    src_port = header[0]
    dst_port = header[1]
    length = header[2]
    checksum = header[3]

    #print("****UDP Header****")
    #print("\tSource port: "+str(src_port))
    #print("\tDestination port: "+str(dst_port))
    #print("\tLength: "+str(length))
    #print("\tChecksum: "+str(checksum))

    application_layer_packet = datagram[8:]
    return application_layer_packet, dst_port

def ipv6_replace_src_addr(ipv6_packet, src_addr):
    src_binary = socket.inet_pton(socket.AF_INET6, src_addr)
    #struct.pack_into("!2Q", ipv6_packet, 8, src_binary[0], src_binary[8])
    new_ipv6_packet = ipv6_packet[:8] + src_binary[:16] + ipv6_packet[24:]
    return new_ipv6_packet

def ipv6_replace_dst_addr(ipv6_packet, dst_addr):
    dst_binary = socket.inet_pton(socket.AF_INET6, dst_addr)
    #struct.pack_into("!2Q", ipv6_packet, 24, dst_binary[0], dst_binary[8])
    new_ipv6_packet = ipv6_packet[:24] + dst_binary[:16] + ipv6_packet[40:]
    return new_ipv6_packet

#only accepts unicast packets so far
def destination_myself(ipv6_packet):
  #extracting my own ip doesnt work somehow, hardcode it
  '''
  my_hostname = socket.gethostname()
  print("hostname: "+str(my_hostname))
  my_ip = socket.getaddrinfo(my_hostname, None, socket.AF_INET6)[0][4][0]
  print("my hostname: "+my_hostname+" ,my ipv6 addr: "+my_ip)
  '''
  transport_layer_datagram, next_hdr, src_addr, dst_addr = ipv6_header_parse(bytes(ipv6_packet))
  dst_addr = ipaddress.IPv6Address(dst_addr)
  print("destination: "+str(dst_addr)+", my ip: "+str(er_public_ipv6_address))
  if(dst_addr == er_public_ipv6_address):
    return True
  else:
    return False

def destination_cloudserver(ipv6_address):
  dst_addr = ipaddress.IPv6Address(ipv6_address)
  #print("destination: "+str(dst_addr)+", server addr: "+str(cloud_server_ipv6_address))
  if(dst_addr == cloud_server_ipv6_address):
    return True
  else:
    return False