# Bolt interface for BaseBoard
#
# Author: abiri
# Date:   07.11.19

import spidev
import time
from enum import Enum
from datetime import datetime
import gpiod


# Structures -----------------------------------------------------------------------------------------------------------

class GPIOPin:
    PORT = ''
    PIN  = 0

    def __init__(self, port, pin):
        self.PORT = port
        self.PIN  = pin

    def __hash__(self):
        return hash((self.PORT, self.PIN))

    def __eq__(self, other):
        return (self.PORT, self.PIN) == (other.PORT, other.PIN)

    def __ne__(self, other):
        return not (self == other)


# DEFINES --------------------------------------------------------------------------------------------------------------

# Constants
class BoltMode(Enum):
    READ = 0
    WRITE = 1


# Colibri iMX7
BOLT_APP_ACK  = GPIOPin("gpiochip2", 17)
BOLT_APP_REQ  = GPIOPin("gpiochip3", 11)
BOLT_APP_MODE = GPIOPin("gpiochip2", 18)
BOLT_APP_IND  = GPIOPin("gpiochip6", 3)

class BoltSPI(Enum):
    BUS    = 2
    DEVICE = 0

# Colibri iMX6ULL
#BOLT_APP_ACK  = GPIOPin("gpiochip2", 17)
#BOLT_APP_REQ  = GPIOPin("gpiochip2", 26)
#BOLT_APP_MODE = GPIOPin("gpiochip2", 18)
#BOLT_APP_IND  = GPIOPin("gpiochip1", 0)

#class BoltSPI(Enum):
#    BUS    = 0
#    DEVICE = 0


# LIBGPIOD wrapper -----------------------------------------------------------------------------------------------------
consumer_name = "bolt_interface.py"

# Line dictionary for libgpiod
line = {}


def gpio_init():

    # Setting up dictionary for quick access to GPIO lines & opening corresponding chips

    # Setup lines for gpioset
    gpio_add_output(BOLT_APP_REQ)
    gpio_add_output(BOLT_APP_MODE)

    # Setup lines for gpioget
    gpio_add_input(BOLT_APP_ACK)
    gpio_add_input(BOLT_APP_IND)


def gpio_deinit():

    # Release the lines again
    for line_val in line.values():
        chip_temp = line_val.owner()
        line_val.release()
        chip_temp.close()

    line.clear()

    if len(line):
        print("WARNING: Open lines remaining!")


def gpio_add_output(gpio):
    line_temp = gpiod.Chip(gpio.PORT).get_line(gpio.PIN)
    line_temp.request(consumer=consumer_name, type=gpiod.LINE_REQ_DIR_OUT)
    line[gpio] = line_temp


def gpio_add_input(gpio):
    line_temp = gpiod.Chip(gpio.PORT).get_line(gpio.PIN)
    line_temp.request(consumer=consumer_name, type=gpiod.LINE_REQ_DIR_IN)
    line[gpio] = line_temp


def gpioset(gpio, value):

    # Write to GPIO
    line_temp = gpiod.Chip(gpio.PORT).get_line(gpio.PIN)
    line_temp.request(consumer=consumer_name, type=gpiod.LINE_REQ_DIR_OUT)
    line_temp.set_value(value)

    #line_temp.release()
    #gpiod.Chip(gpio.PORT).close()

    # Write to GPIO - FIXME: Only works for a single run of the script
    #line[gpio].set_value(value)


def gpioget(gpio):

    # Read from GPIO
    line_temp = gpiod.Chip(gpio.PORT).get_line(gpio.PIN)
    line_temp.request(consumer=consumer_name, type=gpiod.LINE_REQ_DIR_IN)
    val = line_temp.get_value()

    #line_temp.release()
    #gpiod.Chip(gpio.PORT).close()

    # Read from GPIO - FIXME: Only works for a single run of the script
    #val = line[gpio].get_value()

    return int(val)


# SPI ------------------------------------------------------------------------------------------------------------------

# Setup SPI connection
def bolt_init():
    spi = spidev.SpiDev()
    spi.open(BoltSPI.BUS.value, BoltSPI.DEVICE.value)
    spi.max_speed_hz = 4000000 # 4 MHz
    spi.mode = 0b00 # CPOL = 0, CPHA = 0

    # Setup Libgpiod - FIXME: Disabled because of issue with running script multiple times
    #gpio_init()

    return spi


def bolt_deinit(spi):

    # De-init Libgpiod - FIXME: Disabled because of issue with running script multiple times
    #gpio_deinit()

    spi.close()


def bolt_select():
    # Pull CS high
    # Not performed with BOLT -> see bolt_request
    None


def bolt_deselect():
    # Pull CS low
    # Not performed with BOLT -> see bolt_release
    None

# BOLT -----------------------------------------------------------------------------------------------------------------


def bolt_setmode(mode):

    # Set BOLT_MODE to correct level
    gpioset(BOLT_APP_MODE, mode)


def bolt_request():

    # Set request line
    gpioset(BOLT_APP_REQ, 1)

    # Wait for acknowledgement
    cnt = 1
    ack = bool(gpioget(BOLT_APP_ACK))
    while not ack:
        time.sleep(0.001)
        ack = bool(gpioget(BOLT_APP_ACK))
        if cnt >= 5:
            print("WARNING: bolt_request: timeout on ack")
            return False
        cnt += 1
        
    return True


def bolt_release():

    # Clear request line
    gpioset(BOLT_APP_REQ, 0)


def bolt_check_ind():

    # Read Indication line
    return bool(gpioget(BOLT_APP_IND))


# Receive BOLT messages
def bolt_rx(spi):

    bolt_select()
    bolt_setmode(BoltMode.READ.value)

    # Alert BOLT
    if not bolt_request():
        return None
    
    # Read while ACK line is high, byte by byte
    raw_message = []
    ack = True
    while ack:
        raw_message += spi.readbytes(1)

        # Verify ACK is still high
        ack = bool(gpioget(BOLT_APP_ACK))
        #print(str(raw_message))

    # Free BOLT
    bolt_release()
    bolt_deselect()

    #remove BOLT header: 1 byte length field
    data_len = raw_message[0]
    message = raw_message[1:data_len+1]

    return message


# Listen for and print BOLT messages
def bolt_listen(spi):

    # Use CS to enable BOLT
    bolt_select()

    # Set BOLT mode to read
    bolt_setmode(BoltMode.READ.value)

    while bolt_check_ind():

        message = bolt_rx(spi)
        
        stringlist = [chr(x) for x in message]

        print('message: ' + ''.join(stringlist) + " (" + str(message) + ")")

    # Free SPI again
    bolt_deselect()
    
    return True


# Write message to BOLT
def bolt_tx(spi, message, length):

    # Use CS to enable BOLT
    bolt_select()

    # Set BOLT mode to write
    bolt_setmode(BoltMode.WRITE.value)

    # Alert BOLT
    if not bolt_request():
        return False

    # Note: Currently, we transmit a string; if this were a byte array, the length is given as a second parameter
    # Transform string into a byte literal and then into a list of integers
    #message_list = list(message.encode())
    #message_list = list([int.from_bytes(message[i:i+1],"big") for i in range(len(message))])
    message_list = []
    #add BOLT 1 byte header field: data length
    message_list.append(len(message))
    for i in range(len(message)):
        #print("i: "+str(i))
        message_list.append(int.from_bytes(message[i:i+1],"big"))

    #print("message list len: "+str(len(message_list)))
    #message_list = message

    #print("type message: "+str(type(message))+" type of 1 element: "+str(type(message[0])))
    #print("type of message_list: "+str(type(message_list)))

    # Send message
    spi.writebytes(message_list)
    #for k in range(len(message_list)):
    #    print("k: "+str(k))
    #    print(str(message_list[k]))
    #    next_byte = [message_list[k]]
    #    spi.writebytes(next_byte)

    print("written to bolt")

    # Free BOLT
    bolt_release()

    # Free BOLT again
    bolt_deselect()
    
    return True

# HELPER ---------------------------------------------------------------------------------------------------------------


# Time for debugging
t_start = datetime.now()
t_delta = t_start


def print_time_diff(identifier):
    global t_start  # Print difference to start of script
    global t_delta  # Print difference to last call of this function

    temp = datetime.now()
    diff_start = temp - t_start
    diff_delta = temp - t_delta

    print("Point: %8s, time: %10d, delta: %10d" % (str(identifier), diff_start.microseconds + diff_start.seconds * 10**6, diff_delta.microseconds + diff_delta.seconds * 10**6))

    # Store time as new t_delta so we can compute the difference between prints
    t_delta = temp

# MAIN -----------------------------------------------------------------------------------------------------------------

def test_bolt():
    # Initialize BOLT
    print("INFO: Initializing BOLT")
    spi_handle = bolt_init()

    # Wait for messages
    print("INFO: Listening for BOLT messages...")
    bolt_listen(spi_handle)

    print("INFO: Finished listening for BOLT messages")

    # Test by writing a message
    message_test = "Testing BOLT write"
    bolt_tx(spi_handle, message_test, len(message_test))

    print("INFO: Finished writing a BOLT message")

    bolt_deinit(spi_handle)
    print("INFO: Closed BOLT")
