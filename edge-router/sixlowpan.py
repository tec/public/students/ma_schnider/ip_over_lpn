from scapy.layers.sixlowpan import *
from scapy.layers.dot15d4 import *
from scapy.layers.inet6 import *
from scapy.all import *
from scapy.utils import *
import binascii
import struct
import socket
import wan_eth_interface as wan_interface

ipv6_uncompressed_dispatch_type = 0x41
#link_local_prefix = "fe80:0000:0000:0000"
link_local_prefix = "fc00:0000:0000:0000"

class TagCounterSixlowpan:
    tag_id = 1

class L2SequenceNumber:
    seq_num = 1
    def get_next_seqnum(self):
        next_seqnum = self.seq_num
        if(self.seq_num < 250):
            self.seq_num = self.seq_num+1
        else:
            self.seq_num = 1
        return next_seqnum

l2_seqnum_counter = L2SequenceNumber()

def get_mac_from_ipv6(ipv6_address):
    address_binary = socket.inet_pton(socket.AF_INET6, ipv6_address)
    mac = bytearray(address_binary[8:])
    mac[0] = 0x02
    return mac

def prepare_fragments(packet):
    #ipv6_packet = LoWPAN_IPHC()/IPv6(packet,dst="fe80::1")
    #ipv6_packet = LoWPANUncompressedIPv6(_type=ipv6_uncompressed_dispatch_type)/IPv6(packet,dst="fe80::1")
    ipv6_packet = LoWPANUncompressedIPv6(_type=ipv6_uncompressed_dispatch_type)/IPv6(packet)
    #is_ping_packet = False

    #bugix: drop packet that are sent back with dst user
    if(wan_interface.destination_cloudserver(ipv6_packet[IPv6].dst)):
        print("input has user dst, dropping")
        return None

    #recalculate udp checksum
    if(ipv6_packet.haslayer(UDP)): 
        del ipv6_packet[UDP].chksum
        ipv6_packet.show2(dump=True)
        #ipv6_packet.show2()
    #elif(ipv6_packet.haslayer(ICMPv6EchoRequest)):
        #print("ping packet binary: "+str(binascii.hexlify(bytes(ipv6_packet))))
        #print("only icmp layer: " + str(binascii.hexlify(bytes(ipv6_packet[ICMPv6EchoRequest]))))
        #ipv6_packet.show2()
        #is_ping_packet = True
    else:
        print("not a udp or ping packet")
        #ipv6_packet.show2()
        #return None
    #iphc_packet = LoWPAN_IPHC()/ipv6_packet
    fragments = sixlowpan_fragment(ipv6_packet, datagram_tag=TagCounterSixlowpan.tag_id)

    #for k in fragments:
        #k.dam = 0x0
        #k.dac = 0x0
        #k.sam = 0x0
        #k.sac = 0x0
        #k.show()
    if (TagCounterSixlowpan.tag_id < 250):
        TagCounterSixlowpan.tag_id = TagCounterSixlowpan.tag_id + 1
    else:
        TagCounterSixlowpan.tag_id = 1
    #framed_fragments = [Dot15d4(fcf_srcaddrmode=3, fcf_destaddrmode=3, seqnum=L2SequenceNumber.seq_num)/Dot15d4Data(dest_panid=0xABCD, src_panid=0xABCD)/x for x in fragments]
    #framed_fragments = [Dot15d4(fcf_srcaddrmode=3,fcf_destaddrmode=3,seqnum=L2SequenceNumber.seq_num)/Dot15d4Data(dest_panid=0xABCD, src_panid=0xABCD, src_addr=0x0200000000000003, dest_addr=0x0200000000000001)/x for x in fragments]
    framed_fragments = []
    for x in fragments:
        #if(is_ping_packet):
            #print("ping fragment binary: " + str(binascii.hexlify(bytes(x))))
            #print("payload binary: "+ str(binascii.hexlify(bytes(x[Raw]))))
        mac_seq_num = l2_seqnum_counter.get_next_seqnum()
        dest_mac = int.from_bytes(get_mac_from_ipv6(ipv6_packet[IPv6].dst),"big")
        #next_framed_fragment = Dot15d4(fcf_srcaddrmode=3,fcf_destaddrmode=3,seqnum=mac_seq_num)/Dot15d4Data(dest_panid=0xABCD, src_panid=0xABCD, src_addr=0x0200000000000003, dest_addr=0x0000000000000000)/x
        next_framed_fragment = Dot15d4(fcf_srcaddrmode=3,fcf_destaddrmode=3,seqnum=mac_seq_num)/Dot15d4Data(dest_panid=0xABCD, src_panid=0xABCD, src_addr=0x0200000000000003, dest_addr=dest_mac)/x
        next_framed_fragment.show2()
        if(next_framed_fragment.haslayer(Raw)):
            print("fragment payload (coap) size: "+str(len(bytes(next_framed_fragment[Raw]))))
        framed_fragments.append(next_framed_fragment)
    '''
    if (L2SequenceNumber.seq_num < 250):
        L2SequenceNumber.seq_num = L2SequenceNumber.seq_num +1
    else:
        L2SequenceNumber.seq_num = 1
    '''
    #framed_fragments[0].show2()
    #framed_fragments[0].show()
    print("created "+str(len(fragments))+" fragments")
    return framed_fragments

def insert_sorted(frag_same_tag_list, fragment, fragment_offset):
    list_element_offset = None
    for u in range(len(frag_same_tag_list)):
        if(LoWPANFragmentationFirst in frag_same_tag_list[u]):
            list_element_offset = 0
        elif(LoWPANFragmentationSubsequent in frag_same_tag_list[u]):
            list_element_offset = frag_same_tag_list[u][LoWPANFragmentationSubsequent].datagramOffset
        if(fragment_offset < list_element_offset):
            frag_same_tag_list.insert(u,fragment)
            return frag_same_tag_list

    frag_same_tag_list.append(fragment)
    return frag_same_tag_list

#fragment list is has nested lists containing fragments of the same tag
#returns: updated fragment list, index of list to which fragment was added, True/False if list is completed
def add_fragment_to_list(fragment_list, fragment):
    target_tag = None
    target_offset = None #need to multiply this value by 8 to get offset in bytes
    target_datagram_size = None
    target_fragment_size = None
    is_last_fragment = False

    if(LoWPANFragmentationFirst in fragment):
        target_tag = fragment[LoWPANFragmentationFirst].datagramTag
        target_offset = 0
        target_datagram_size = fragment[LoWPANFragmentationFirst].datagramSize
        target_fragment_size = len(fragment[LoWPANFragmentationFirst].payload.load)
    elif(LoWPANFragmentationSubsequent in fragment):
        target_tag = fragment[LoWPANFragmentationSubsequent].datagramTag
        target_offset = fragment[LoWPANFragmentationSubsequent].datagramOffset
        target_datagram_size = fragment[LoWPANFragmentationSubsequent].datagramSize
        target_fragment_size = len(fragment[LoWPANFragmentationSubsequent].payload.load)

    if(target_offset * 8 + target_fragment_size >= target_datagram_size):
        is_last_fragment = True

    #debug
    #print("fragment: offset="+str(target_offset)+", fragment size="+str(target_fragment_size)+" , datagram size="+str(target_datagram_size)+", last fragment="+str(is_last_fragment))

    if(target_tag is None):
        print("cant add fragment to list, no sixlowpan layer")
        return

    list_tag = None
    tag_found = False

    for i in range(len(fragment_list)):
        if(len(fragment_list[i]) > 0):
            sixlowpan_sublayer = None
            if(LoWPANFragmentationFirst in fragment_list[i][0]):
                sixlowpan_sublayer = LoWPANFragmentationFirst
            elif(LoWPANFragmentationSubsequent in fragment_list[i][0]):
                sixlowpan_sublayer = LoWPANFragmentationSubsequent
            list_tag = fragment_list[i][0][sixlowpan_sublayer].datagramTag
            if(list_tag == target_tag):
                fragment_list[i] = insert_sorted(fragment_list[i],fragment, target_offset)
                return fragment_list,i,is_last_fragment


    #no entries found in list for this tag, create new nested list
    fragment_list.append(list(fragment))
    index = len(fragment_list)-1
    return fragment_list,index,is_last_fragment


def reassemble_fragments(framed_fragments):
    #fragments = framed_fragments[Dot15d4Data].payload
    reassembled_fragments = sixlowpan_defragment(framed_fragments)
    return reassembled_fragments

#fix bug: sixlowpan layer not uncompressing the addresses
def uncompress_addresses(compressed_packet,mac_address_src,mac_address_dst):
    if(not compressed_packet.haslayer(IPv6)):
        print("WARN: received packet with no ipv6 layer. Probably a fragment was lost")
        #compressed_packet.show2()
        return None
    src_ip = compressed_packet[IPv6].src
    dst_ip = compressed_packet[IPv6].dst
    #print("packet source: "+str(src_ip)+", type="+str(type(src_ip)))
    if((src_ip is None) or (src_ip == "::") or (src_ip == 0)):
        #print("packet src: "+str(mac_address_src) + ", dst: "+str(mac_address_dst))
        #print("address type="+str(type(mac_address_src)))
        src_mac_hex = hex(mac_address_src)[2:]
        num_missing_zeros = 16 - len(src_mac_hex)
        while (num_missing_zeros > 0):
            src_mac_hex = "0"+src_mac_hex
            num_missing_zeros = num_missing_zeros - 1

        #need to invert the local bit
        src_iid = src_mac_hex[0] + hex(int(src_mac_hex[1],base=16) ^ 0x2)[2:] + src_mac_hex[2:]
        link_local_ip_src = link_local_prefix + ":"+src_iid[:4]+":"+src_iid[4:8]+":"+src_iid[8:12]+":"+src_iid[12:16]
        #print("converted src mac: "+src_mac_hex)
        #print("decompressed src addr: "+link_local_ip_src)
        compressed_packet[IPv6].src = link_local_ip_src

    if(compressed_packet.haslayer(UDP)):
        del compressed_packet[UDP].chksum
        compressed_packet.show2(dump=True)

    return compressed_packet