# Host System

This code was executed on the Baseboard of the edge router device. It runs a linux distribution based on the [Yocto Project](https://www.yoctoproject.org/). Python3 and the Scapy library are required. In the scapy library, in file scapy/layers/sixlowpan.py the `MAX_SIZE` argument has to be manually decreased to leave enough room for headers on the layers below 6LoWPAN (value of 91 used in this thesis).

# Usage

The edge router operation can be started by executing the command: `python3 baseboard_router.py`