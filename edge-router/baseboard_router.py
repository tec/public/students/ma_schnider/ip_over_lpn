#main loop for the baseboard routing
#
# Author: sraphael
# Date:   13.12.19

import bolt_interface as bolt
import wan_eth_interface as wan_interface
import sixlowpan as sixlowpan
from scapy.all import *
from scapy.utils import *
import time
import binascii

#edge_router_ipv4_address = "82.130.102.49"
edge_router_ipv6_address = "2001:67c:10ec:2a49:8000::23"
#user_ipv4_address = "82.130.102.108"
#following addresses for direct switch connection
edge_router_ipv4_address = "192.168.1.1"
user_ipv4_address = "192.168.1.2"
next_hdr_ipv6_code = 0x29


bolt_spi_handle = bolt.bolt_init()
wan_socket = wan_interface.wan_interface_init()
fragment_reassembly_list = []
bind_layers(Dot15d4Data, SixLoWPAN)
print ("starting ipv6 sniffer")


while True:
    net_packet,net_proto = wan_interface.wan_receive(wan_socket)
    if net_packet is not None:
        if (net_proto == 'IPV6'): #we are only interested in tunneled ipv6 packets
            print ("received untunneled ipv6 packet")
            #print(binascii.hexlify(net_packet))
            
        elif (net_proto == 'IPV4'):
            datagram, transport_proto,src_ip, dst_ip = wan_interface.ipv4_header_parse(net_packet)
            #only want encapsulated packets
            if (transport_proto == next_hdr_ipv6_code):
                enc_datagram, enc_transport_proto,enc_src_ip, enc_dst_ip = wan_interface.ipv6_header_parse(datagram)

                

                print("received tunnel packet")
                fragments = sixlowpan.prepare_fragments(datagram)
                if(fragments is not None):
                    for x in fragments:
                        y = bytes(x)
                        bolt_success = bolt.bolt_tx(bolt_spi_handle, y, len(y))
                        if (bolt_success == False):
                            print ("bolt tx failed")
    while bolt.bolt_check_ind():
        #print("receiving fragment from bolt")
        raw_framed_fragment = bolt.bolt_rx(bolt_spi_handle)
        #print("received fragment, parsing now")
        framed_fragment = Dot15d4(raw_framed_fragment)
        #framed_fragment.show2()
        #print("received fragment: "+str(binascii.hexlify(bytes(framed_fragment[Dot15d4Data].payload))))
        if((LoWPANFragmentationFirst in framed_fragment) or (LoWPANFragmentationSubsequent in framed_fragment)):
            fragment_reassembly_list, index, last_fragment = sixlowpan.add_fragment_to_list(fragment_reassembly_list, framed_fragment)
            if(last_fragment is True):
                print("reassembled a packet, consisting of "+ str(len(fragment_reassembly_list[index]))+" fragments")
                #print(fragment_reassembly_list[index])
                try:
                    reassembled_fragments = sixlowpan.reassemble_fragments(fragment_reassembly_list[index])
                    del fragment_reassembly_list[index]
                except:
                    e = sys.exc_info()[0]
                    print("unexpected error: %s" % e)
                    del fragment_reassembly_list[index]
                    continue
                #print(reassembled_fragments)
                for key in reassembled_fragments:
                    compressed_ipv6_packet = reassembled_fragments[key]
                    #print(type(compressed_ipv6_packet))
                    #print(str(binascii.hexlify(bytes(compressed_ipv6_packet))))
                    #compressed_ipv6_packet.show2()
                    mac_address_src = framed_fragment[Dot15d4Data].src_addr
                    mac_address_dst = framed_fragment[Dot15d4Data].dest_addr
                    compressed_ipv6_packet = sixlowpan.uncompress_addresses(compressed_ipv6_packet, mac_address_src, mac_address_dst)
                    if(compressed_ipv6_packet is not None):
                        uncompressed_ipv6_packet = compressed_ipv6_packet[IPv6]
                    else:
                        continue

                    if(wan_interface.destination_myself(uncompressed_ipv6_packet)):
                        print("received fragmented packet from subnet addressed to edge router")
                        

                    else:
                        '''
                        ipv6_src_replaced = wan_interface.ipv6_replace_src_addr(bytes(uncompressed_ipv6_packet),edge_router_ipv6_address)
                        ipv6_src_replaced = IPv6(ipv6_src_replaced)
                        del ipv6_src_replaced[UDP].chksum
                        ipv6_src_replaced.show2()
                        send(ipv6_src_replaced)
                        '''
                        
                        tunneled_packet = IP(src=edge_router_ipv4_address, dst=user_ipv4_address)/IPv6(uncompressed_ipv6_packet)
                        print("created tunnel packet:")
                        tunneled_packet.show2()
                        send(tunneled_packet)
                        
                        
        else:
            if(framed_fragment.haslayer(IPv6)):
                uncompressed_ipv6_packet = framed_fragment[IPv6]
                mac_address_src = framed_fragment[Dot15d4Data].src_addr
                mac_address_dst = framed_fragment[Dot15d4Data].dest_addr
                framed_fragment = sixlowpan.uncompress_addresses(framed_fragment, mac_address_src, mac_address_dst)
                if(wan_interface.destination_myself(uncompressed_ipv6_packet)):
                    print("received unfragmented packet from subnet addressed to edge router")
                    
                else:
                    '''
                    ipv6_src_replaced = wan_interface.ipv6_replace_src_addr(bytes(uncompressed_ipv6_packet),edge_router_ipv6_address)
                    ipv6_src_replaced = IPv6(ipv6_src_replaced)
                    del ipv6_src_replaced[UDP].chksum
                    ipv6_src_replaced.show2()
                    send(ipv6_src_replaced)
                    '''
                    
                    tunneled_packet = IP(src=edge_router_ipv4_address, dst=user_ipv4_address)/IPv6(uncompressed_ipv6_packet)
                    print("created tunnel packet:")
                    tunneled_packet.show2()
                    send(tunneled_packet)
            else:
                continue
                    
        
