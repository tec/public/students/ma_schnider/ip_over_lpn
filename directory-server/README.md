# Host System

The directory server was executed on a laptop running Ubuntu 18.04. Python3 and the CoAPthon3 library are needed to execute the directory server.

# Usage

The directory server can be started by using the command: `python3 directory_server.py`