from coapthon.server.coap import CoAP
from directory_resources import *
import sys
import logging

class CoAPServer(CoAP):
    def __init__(self, host, port, multicast=False):
        CoAP.__init__(self, (host, port), multicast)
        self.add_resource('devices/', RegisteredDevices())

        print(("CoAP Server start on " + host + ":" + str(port)))
        print((self.root.dump()))


def main(argv):
    logger=logging.getLogger()
    logger.setLevel(logging.INFO)
    ip = "::"
    port = 5683
    multicast = False
    server = CoAPServer(ip, port, multicast)
    while(True):
        try:
            server.listen(10)
        except KeyboardInterrupt:
            print("Server Shutdown")
            server.close()
            print("Exiting...")
            break

if __name__ == "__main__":
    main(sys.argv[1:])