from coapthon import defines

from coapthon.resources.resource import Resource

registed_devices_dict = []

class RegisteredDevices(Resource):
    #registed_devices_dict = {}
    
    def __init__(self, name="RegisteredDevices", coap_server=None):
        super(RegisteredDevices, self).__init__(name, coap_server, visible=True, observable=False, allow_children=True)
        self.payload = "This is a test"
        self.content_type = "text/plain"

    def add_device_to_dict(self, eui64, ipv6_address):
        registered_devices_dict[eui64] = ipv6_address

    def get_device_from_dict(self, eui64):
        if(eui64 == 0):
            print("something to do here")
        elif(eui64 in registered_devices_dict):
            #use eui64 to get the whole list
            payload = "{\""+str(eui64)+"\":\""+registered_devices_dict[eui64]+"\"}"
            content_type = "text/json"
        else:
            #no entry for this eui64
            print("WARN: no entry for this eui64: "+ str(eui64))

    def get_device_list(self):
        self.payload = str(registered_devices_dict)
        return self


    def render_GET(self, request):
        self.payload = str(registed_devices_dict)
        return self

    def render_PUT(self, request):
        print("received put request")
        #print(request)
        #self.edit_resource(request)
        if not str(request.payload) in registed_devices_dict:
            registed_devices_dict.append(str(request.payload))
        print("new database: " + str(registed_devices_dict))
        return self

    def render_POST(self, request):
        print("received post request")
        #print(request)
        #self.edit_resource(request)
        if not str(request.payload) in registed_devices_dict:
            registed_devices_dict.append(str(request.payload))
        print("new database: " + str(registed_devices_dict))
        return self